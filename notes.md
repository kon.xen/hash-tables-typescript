# Notes

**To create a typescript project from scratch :**
[typescript.tv](
 https://typescript.tv/hands-on/fastest-way-to-set-up-a-typescript-project-with-nodejs-npm)

* `git init1`
* `npm init`
* `npm install typescript`
* `npx tsc --init` to create a config for the compiler
* `npx tsc` compiles
* `node index.js` runs the programme

Should include tsc in the *package.json* under *scripts*, you can then 
`npm run compile`

`
"scripts": {
"compile": "tsc"
},
`

Everytime ypu compile the compiled code goes in to the `index.js` file.
you also need an *`index.ts`* for the actual code.



