"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.HashTable = void 0;
class HashTable {
    constructor(size) {
        this.data = new Array(size);
    }
    _hash(key) {
        let hash = 0;
        for (let i = 0; i < key.length; i++) {
            hash = (hash + key.charCodeAt(i) * i) % this.data.length;
        }
        return hash;
    }
    set(key, value) {
        let index = this._hash(key);
        this.data[index] = [key, value];
    }
    get(key) {
        let index = this._hash(key);
        return this.data[index];
    }
}
exports.HashTable = HashTable;
