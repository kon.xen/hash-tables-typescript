export class HashTable {

    private readonly data: any[];

    constructor(size:number) {
        this.data = new Array(size);
    }

    _hash(key:any) {
        let hash = 0;
        for (let i =0; i < key.length; i++){
            hash = (hash + key.charCodeAt(i) * i) % this.data.length
        }
        return hash;
    }

    set(key:any, value:any) {
        let index = this._hash(key);
        this.data[index]=[key,value];
    }

    get(key:any) {
        let index = this._hash(key);
        return this.data[index];
    }
}

